module.exports = {
  content: ["**/*.twig"],
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
  experimental: {
    optimizeUniversalDefaults: true
  },
  darkMode: 'class',
};
