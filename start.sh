#!/usr/bin/env bash
set -e

rm -r .git

NAME=$1
mv tailwind.info.yml "${NAME}".info.yml
mv tailwind.libraries.yml "${NAME}".libraries.yml
mv tailwind.theme "${NAME}".theme

#Todo, serach "tailwind" and renplace by NAME